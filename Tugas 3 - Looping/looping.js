console.log ('No. 1 LOOPING WHILE');

console.log ('LOOPING PERTAMA');

var flag = 2;
while (flag < 21) {
  console.log (flag + ' - I love coding');
  flag +=2;
}

console.log ('LOOPING KEDUA');

var flag = 20;
while (flag > 0) {
  console.log (flag + ' - I will become a mobile developer');
  flag -=2;
}

console.log ('No. 2 LOOPING FOR');

for (var angka = 1; angka < 21; angka++) {
  if (angka % 2 == 0) {
    console.log (angka + ' - Berkualitas');
  }
  if (angka % 2 != 0) {
    console.log (angka + ' - Santai');
  }
  if (angka % 3 == 0 && angka % 2 != 0) {
    console.log (angka + ' - I Love Coding');
  }
}


console.log ('No. 3 PERSEGI PANJANG #');

var txt = '####';
var x;

for (x of txt) {
  console.log(x + '#######');
}


console.log ('No. 4 Membuat Tangga')

// let tangga = ['#', '##', '###', '####', '#####', '######', '#######','########'];

//for (let i = 0; i < tangga.length; i++) {
//  console.log (tangga[i])
//}

var tanga = '#'
for (var a = 0; a < 6; a++) {
  console.log (tanga);
  tanga = tanga + '#';
}
console.log (tanga);

console.log ('No. 5 Membuat Papan Catur')

var catur = ''
for (var x = 0; x < 8; x++) {
  for (var y = 0; y < 8; y++) {
    if ((y % 2 == 0 && x % 2 == 0) || (x % 2 == 1 && y % 2 == 1)){
      catur += ' '
    } else {
      catur += '#'
    }
  }
  catur += '\n'
}
console.log(catur);

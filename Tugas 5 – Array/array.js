console.log('SOAL 1')
console.log('========')

// Code di sini
function range(startNum, finishNum) {
  var output = []
  if (startNum < finishNum) {
    for (var i = startNum; i<=finishNum; i++){
      output.push(i);
    }
    return output;
  }
  else if (startNum > finishNum) {
    for (var j = startNum; j >= finishNum; j--) {
      output.push(j)
    }
    return output;
  }
  else {
    return -1
  }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log('========')
console.log('SOAL 2')
console.log('========')

// Code di sini

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log('========')
console.log('SOAL 3')
console.log('========')



console.log('========')
console.log('SOAL 4')
console.log('========')

function balikKata(kataawal) {
  var kataaakhir = '';
  for (var i = kataawal.length; i >= 0; i--) {
    kataaakhir = kataaakhir + kataawal.charAt(i);
  }
  return kataaakhir;
}
// Code di sini

console.log(balikKata('''Kasur Rusak''') // kasuR rusaK
console.log(balikKata("SanberCode") // edoCrebnaS
console.log(balikKata("Haji Ijah") // hajI ijaH
console.log(balikKata("racecar") // racecar
console.log(balikKata("I am Sanbers") // srebnaS ma I

function SoalIfElse(nama, peran) {
  if (nama == '') {
    console.log ('Nama harus diisi!')
  } else if (nama && peran == '') {
    console.log ('Halo ' + nama + ", Pilih peranmu untuk memulai game!")
  } else if (nama == 'Jane' && peran == 'Penyihir') {
    console.log ('Selamat datang di Dunia Werewolf, Jane \n Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!')
  } else if (nama == 'Jenita' && peran == 'Guard') {
    console.log ('Selamat datang di Dunia Werewolf, Jenita \n Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.')
  } else if (nama == 'Junaedi' && peran == 'Werewolf') {
    console.log ('Selamat datang di Dunia Werewolf, Junaedi \n Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
  }
}

console.log ('Soal IfElse 1 ===============')
SoalIfElse ('', '')
console.log ('Soal IfElse 2 ===============')
SoalIfElse ('John', '')
console.log ('Soal IfElse 3 ===============')
SoalIfElse ('Jane', 'Penyihir')
console.log ('Soal IfElse 4 ===============')
SoalIfElse ('Jenita', 'Guard')
console.log ('Soal IfElse 5 ===============')
SoalIfElse ('Junaedi', 'Werewolf')

console.log ('Soal SwitchCase 1 ===============')

var tanggal = 2
var bulan = 10
var tahun = 1990
var TeksBulan;

switch (true) {
  case (tanggal < 1 || tanggal > 31):
    console.log ('Input tanggal salah')
    break;
  case (tahun < 1990 || tahun > 2200):
    console.log ('Input tanggal salah')
    break;
  default:
  {
    switch (true) {
      case bulan == 1:
        TeksBulan = 'Januari';
        break;
        case bulan == 2:
          TeksBulan = 'Februari';
          break;
          case bulan == 3:
            TeksBulan = 'Maret';
            break;
            case bulan == 4:
              TeksBulan = 'April';
              break;
              case bulan == 5:
                TeksBulan = 'Mei';
                break;
                case bulan == 6:
                  TeksBulan = 'Juni';
                  break;
                  case bulan == 7:
                    TeksBulan = 'Juli';
                    break;
                    case bulan == 8:
                      TeksBulan = 'Agustus';
                      break;
                      case bulan == 9:
                        TeksBulan = 'September';
                        break;
                        case bulan == 10:
                          TeksBulan = 'Oktober';
                          break;
                          case bulan == 11:
                            TeksBulan = 'November';
                            break;
                            case bulan == 12:
                              TeksBulan = 'Desember';
                              break;

      default:
        break;
    }
    console.log (tanggal, '', TeksBulan, '', tahun)
    break;
  }

}

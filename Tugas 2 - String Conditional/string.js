var Soal1 = "Soal 1"
console.log (Soal1)

var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth= 'I';
var sixth = 'love';
var seventh = 'it';

const kalimat = `${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`

console.log(kalimat)

var Soal2 = "Soal 2"
console.log (Soal2)

var sentence = "I am going to be React Native Developer"
var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var exampleThirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var exampleFourthWord = sentence[11] + sentence[12];
var exampleFifthWord = sentence[14] + sentence[15];
var exampleSixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var exampleSeventWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var exampleEightWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log ('First Word: ' + exampleFirstWord);
console.log ('Second Word: ' + exampleSecondWord);
console.log ('Third Word: ' + exampleThirdWord);
console.log ('Fourth Word: ' + exampleFourthWord);
console.log ('Fifth Word: ' + exampleFifthWord);
console.log ('Sixth Word: ' + exampleSixthWord);
console.log ('Seventh Word: ' + exampleSeventWord);
console.log ('Eight Word: ' + exampleEightWord);

var Soal3 = "Soal 3"
console.log (Soal3)

var sentence2 = 'wow JavaScript is so cool';

var firstword2 = sentence2.substr(0, 3);
var secondword2 = sentence2.substr(4, 10);
var thirdword2 = sentence2.substr(15, 2);
var fourthword2 = sentence2.substr(18, 2);
var fifthword2 = sentence2.substr(21, 4);

console.log ('First Word: ' + firstword2);
console.log ('Second Word: ' + secondword2);
console.log ('Third Word: ' + thirdword2);
console.log ('Fourth Word: ' + fourthword2);
console.log ('Fifth Word: ' + fifthword2);

var Soal4 = "Soal 4"
console.log (Soal4)

var firstword2length = firstword2.length;
var secondword2length = secondword2.length;
var thirdword2length = thirdword2.length;
var fourthword2length = fourthword2.length;
var fifthword2length = fifthword2.length;

console.log ('First Word: ' + firstword2 + ", with length: " + firstword2length);
console.log ('Second Word: ' + secondword2 + ", with length: " + secondword2length);
console.log ('Third Word: ' + thirdword2 + ", with length: " + thirdword2length);
console.log ('Fourth Word: ' + fourthword2 + ", with length: " + fourthword2length);
console.log ('Fifth Word: ' + fifthword2 + ", with length: " + fifthword2length);
